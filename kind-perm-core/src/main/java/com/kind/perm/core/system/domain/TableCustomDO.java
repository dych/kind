package com.kind.perm.core.system.domain;

import java.io.Serializable;

import com.kind.common.persistence.PageQuery;

/**
 * 导出的工具类
 * @author Administrator
 *
 */
public class TableCustomDO extends PageQuery implements Serializable{

	// Fields

	/**
	 * @Fields serialVersionUID : 
	 */
	
	private static final long serialVersionUID = 1L;
	private Long id;
	/**
	 * 类型，不同类型对应不同表
	 */
	private Long tbType;
	/**
	 * 顺序号
	 */
	private Short fieldSort;
	/**
	 * 表字段对应的javaBean属性名称
	 */
	private String fieldName;
	/**
	 * 属性类型
	 */
	private Short fieldType;
	/**
	 * 默认的显示名
	 */
	private String fieldTitle;
	/**
	 * 显示的别名，如果没有则显示默认名称
	 */
	private String fieldAnotherTitle;
	/**
	 * 是否导出，1：导出，0：不导出
	 */
	private Short isExport;
	/**
	 * 是否打印,1:打印，0,不打印
	 */
	private Short isPrint;
	/**
	 * 是否显示，1：显示，0：不显示
	 */
	private Short isShow;

	// Constructors

	/** default constructor */
	public TableCustomDO() {
	}

	/** minimal constructor */
	public TableCustomDO(Long tbType, Short fieldSort, String fieldName,
			Short fieldType, String fieldTitle, Short isExport, Short isPrint,
			Short isShow) {
		this.tbType = tbType;
		this.fieldSort = fieldSort;
		this.fieldName = fieldName;
		this.fieldType = fieldType;
		this.fieldTitle = fieldTitle;
		this.isExport = isExport;
		this.isPrint = isPrint;
		this.isShow = isShow;
	}

	/** full constructor */
	public TableCustomDO(Long tbType, Short fieldSort, String fieldName,
			Short fieldType, String fieldTitle, String fieldAnotherTitle,
			Short isExport, Short isPrint, Short isShow) {
		this.tbType = tbType;
		this.fieldSort = fieldSort;
		this.fieldName = fieldName;
		this.fieldType = fieldType;
		this.fieldTitle = fieldTitle;
		this.fieldAnotherTitle = fieldAnotherTitle;
		this.isExport = isExport;
		this.isPrint = isPrint;
		this.isShow = isShow;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public Long getTbType() {
		return this.tbType;
	}

	public void setTbType(Long tbType) {
		this.tbType = tbType;
	}
	public Short getFieldSort() {
		return this.fieldSort;
	}

	public void setFieldSort(Short fieldSort) {
		this.fieldSort = fieldSort;
	}
	public String getFieldName() {
		return this.fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public Short getFieldType() {
		return this.fieldType;
	}

	public void setFieldType(Short fieldType) {
		this.fieldType = fieldType;
	}
	public String getFieldTitle() {
		return this.fieldTitle;
	}

	public void setFieldTitle(String fieldTitle) {
		this.fieldTitle = fieldTitle;
	}
	public String getFieldAnotherTitle() {
		return this.fieldAnotherTitle;
	}

	public void setFieldAnotherTitle(String fieldAnotherTitle) {
		this.fieldAnotherTitle = fieldAnotherTitle;
	}
	public Short getIsExport() {
		return this.isExport;
	}

	public void setIsExport(Short isExport) {
		this.isExport = isExport;
	}
	public Short getIsPrint() {
		return this.isPrint;
	}

	public void setIsPrint(Short isPrint) {
		this.isPrint = isPrint;
	}
	public Short getIsShow() {
		return this.isShow;
	}

	public void setIsShow(Short isShow) {
		this.isShow = isShow;
	}
}
