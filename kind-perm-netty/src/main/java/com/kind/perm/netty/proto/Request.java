package com.kind.perm.netty.proto;

import java.util.Map;

import com.kind.perm.netty.utils.pack.MarshallUtils;
import com.kind.perm.netty.utils.pack.Marshallable;
import com.kind.perm.netty.utils.pack.Pack;
import com.kind.perm.netty.utils.pack.Unpack;

/**
 * 
 * User: 李明
 * Date: 2016/3/9
 * Time: 10:05
 * To change this template use File | Settings | File Templates.
 */
public class Request implements Marshallable {

    protected int messageID;

    private String uri;

    private Map<String, String> paramMap;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Map<String, String> getParamMap() {
        return paramMap;
    }

    public void setParamMap(Map<String, String> paramMap) {
        this.paramMap = paramMap;
    }

    public int getMessageID() {
        return messageID;
    }

    public void setMessageID(int messageID) {
        this.messageID = messageID;
    }

    @Override
    public String toString() {
        return "Req{" +
                "uri='" + uri + '\'' +
                ", paramMap=" + paramMap +
                "} " + super.toString();
    }

    public void marshal(Pack pack) {
        pack.putInt(messageID);
        pack.putVarstr(uri);
        MarshallUtils.packMap(pack, paramMap, String.class, String.class);
    }

    public void unmarshal(Unpack unpack) {
        messageID = unpack.popInt();
        uri = unpack.popVarstr();
        paramMap = MarshallUtils.unpackMap(unpack, String.class, String.class, false);
    }
}
