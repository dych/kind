package com.kind.perm.netty.utils;


import java.util.HashMap;
import java.util.Map;

import org.aeonbits.owner.ConfigFactory;

import com.kind.perm.netty.AppConfig;
import com.kind.perm.netty.EnvEnum;

/**
 * 
 * User: 李明
 * Date: 2016/1/14
 * Time: 12:45
 * To change this template use File | Settings | File Templates.
 */
public class DefConfigFactory {
    public static AppConfig createUATConfig() {
        return createConfig(EnvEnum.UAT.getEnv());
    }

    public static AppConfig createDEVConfig() {
        return createConfig(EnvEnum.DEV.getEnv());
    }

    public static AppConfig createPRODConfig() {
        return createConfig(EnvEnum.PROD.getEnv());
    }

    public static AppConfig createConfig(String env) {
        Map myVars = new HashMap();
        myVars.put("env", env);
        System.setProperty("env", env);
        return ConfigFactory.create(AppConfig.class, myVars);
    }


}
